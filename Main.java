public class Main
{
    public static void main(String[] args)
    {
        int [] array = new int[8];
        System.out.println("Массив случайных чисел:");
        for(int i = 0; i<8; i++)
        {
            array[i] = (int)(Math.random()*(10-1)+1);
            System.out.print(array[i] + " ");
        }
        // проверка массива на наличие строго возрастающей последовуательности
        int check = 0;
        for(int i = 0; i<7; i++)
        {
            if (array[i] < array[i+1])
                check++;
        }
        if (check==7)
            System.out.println("\nМассив является строго возрастающей последовательностью");
        else System.out.println("\nМассив не является строго возрастающей последовательностью");

        // замена элементов с нечётным индексом на ноль
        System.out.println("Изменённый массив:");
        for(int i = 0; i<8; i++)
        {
            if ((i%2)==1)
                array[i]=0;
            System.out.print(array[i] + " ");
        }
    }
}